"use strict";
var projectSection = null, scrollToTopElement = null,preloader = null, header = null, main = null,  tabletMenu = null, sideMenu = null,
lightBox = null,lightBoxClose = null;

(function() {
window.onload = function() {
  //Initialize swiper
  var mySwiper = new Swiper ('.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    autoplay: true,
    spaceBetween: 20,
    slidesPerView: 'auto',
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    }
  });

  function init() {
  load();
  events();
}
  init();
  preloader.style.visibility = "visible";

function load() {
  projectSection = document.getElementById('projects');
  scrollToTopElement = document.getElementById('scroll-to-top');
  preloader = document.getElementById('preloader');
  header = document.getElementById('header');
  main = document.body;
  sideMenu = document.getElementById('sideNav');
  lightBox = document.querySelector('.preloader-lightbox');
  lightBoxClose = lightBox.querySelector('.preloader-lightbox__close');
 }
function events() {
  if (lightBoxClose) {
    lightBoxClose.addEventListener('click', function() {
      if (this.dataset.opened == 'yes') {
        lightBox.classList.add('hidden');
        this.dataset.opened = 'no';
      }
    });
  }
  window.setTimeout(function(){
    main.style.visibility = "visible";
    header.style.visibility = "visible";
    preloader.style.visibility = "hidden";
  },2000);
  window.addEventListener('scroll', function(e) {
  if(this.scrollY >=projectSection.getBoundingClientRect().top - 100) {
    projectSection.style.animationPlayState ="running";
  }
  if(this.scrollY >= (window.outerHeight/2 + (window.outerHeight/5))) {
    scrollToTopElement.classList.add('scroll-to-topv');
  }
 else if(this.scrollY <= (window.outerHeight/2 + (window.outerHeight/5))) {
    scrollToTopElement.classList.remove('scroll-to-topv');
  }
 });
}
}} ())

